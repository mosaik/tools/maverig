import sys

from cx_Freeze import setup, Executable


"""
Setup script for cx_Freeze. Build with:
    $ python setup_cx.py build
"""

base = None
targetName = 'maverig'
if sys.platform == 'win32':
    base = 'Win32GUI'
    targetName += '.exe'

build_exe_options = {
    'include_files': ['README.rst',
                      'LICENSE.txt',
                      './maverig/tests/data',
                      './maverig/scenarios',
                      './maverig/data/configs',
                      './maverig/data/languages',
                      './maverig/data/icons',
                      './maverig/data/temp',
                      './maverig/data/user_guide',
                      './maverig/data/components'],
    'packages': ['pypower',
                 'mosaik_pypower',
                 'zmq',
                 'zmq.utils.garbage',
                 'zmq.backend.cython',
                 'maverig.utils.maverig_csv',
                 'maverig.utils.visSimulator',
                 'maverig.data.components.utils',
                 'scipy.sparse.csgraph._validation',
                 'scipy.special._ufuncs_cxx'],
    'copy_dependent_files': 'True'
}

executables = [
    Executable('./maverig/EntryPoint.py',
               base=base,
               targetName=targetName)
]

setup(name='Maverig',
      version='1.0.5',
      description='Maverig',
      options={'build_exe': build_exe_options},
      executables=executables)