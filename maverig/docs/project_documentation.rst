=====================
Project Documentation
=====================

Here you can download the Project Documentation of the **Maverig Project** (April 2014 - March 2015)
in German language.

The Project Documentation describes the complete software development process of Maverig including the following parts:

    - Project Organization
    - Smart Grid Basics
    - Maverig Requirements
    - Software Concepts and Design
    - Implementation Documentation
    - Validation
    - Outlook

Download (PDF): :download:`deutsch - Projektdokumentation </_downloads/maverig_projectdocumentation_de.pdf>`
