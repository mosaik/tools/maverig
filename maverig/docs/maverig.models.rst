======================
maverig.models
======================


.. contents::
    :local:

``maverig.models.model``
------------------------

.. automodule:: maverig.models.model
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.models.modelGraph``
-----------------------------

.. automodule:: maverig.models.modelGraph
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.models.modelSimulation``
----------------------------------

.. automodule:: maverig.models.modelSimulation
    :members:
    :undoc-members:
    :show-inheritance:
