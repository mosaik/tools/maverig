============================
maverig.views.groups
============================

.. contents::
    :local:

``maverig.views.groups.abstractGroup``
--------------------------------------

.. automodule:: maverig.views.groups.abstractGroup
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.views.groups.iconGroup``
----------------------------------

.. automodule:: maverig.views.groups.iconGroup
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.views.groups.lineGroup``
----------------------------------

.. automodule:: maverig.views.groups.lineGroup
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.views.groups.lineIconGroup``
--------------------------------------

.. automodule:: maverig.views.groups.lineIconGroup
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.views.groups.nodeGroup``
----------------------------------

.. automodule:: maverig.views.groups.nodeGroup
    :members:
    :undoc-members:
    :show-inheritance:
