=============
About Maverig
=============

Maverig is a Graphical User Interface for creation and visualization of Smart-Grid
simulations using the `mosaik`__ framework.
Maverig has been developed by a `project group`__ of students from the
`Carl von Ossietzky University Oldenburg, Germany`__ in cooperation with `OFFIS`__.

__ http://mosaik.offis.de/
__ http://www.uni-oldenburg.de/informatik/studium-lehre/infos-zum-studium/projektgruppen-im-masterstudium/
__ http://www.uni-oldenburg.de/en
__ http://www.offis.de/en

:Note: Maverig is currently in a prototypical state. It may be used for demonstration purposes, but its maintenance and further development is not our top priority right now. Please understand, if the installation guide is not up-to-date with the newest package versions.


Contributors
------------

Erika Root, Gerrit Klasen, Hanno Günther,
Jerome Tammen, Marina Sartison, Marius Brinkmann,
Michael Falk, Rafael Burschik, Rouven Pajewski
Sascha Spengler, Andrianarisoa A. Johary Ny Aina and
Tobias Schwerdtfeger