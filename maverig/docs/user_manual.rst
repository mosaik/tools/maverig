.. _user_manual

===========
User Manual
===========

You can download the User Manual in the following languages:

- :download:`english - User Manual (PDF) </_downloads/maverig_guide_en.pdf>`

- :download:`deutsch - Benutzerhandbuch (PDF) </_downloads/maverig_guide_de.pdf>`
