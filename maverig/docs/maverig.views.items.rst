===========================
maverig.views.items
===========================

.. contents::
    :local:

``maverig.views.items.abstractItem``
------------------------------------

.. automodule:: maverig.views.items.abstractItem
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.views.items.circle``
------------------------------

.. automodule:: maverig.views.items.circle
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.views.items.icon``
----------------------------

.. automodule:: maverig.views.items.icon
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.views.items.line``
----------------------------

.. automodule:: maverig.views.items.line
    :members:
    :undoc-members:
    :show-inheritance:
