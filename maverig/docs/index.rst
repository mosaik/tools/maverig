===================================
Welcome to Maverig's documentation!
===================================

Contents:

.. toctree::
   :maxdepth: 2

   quickstart
   installation
   user_manual
   project_documentation
   source_documentation
   about

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
