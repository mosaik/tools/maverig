====================
maverig.data
====================


.. toctree::
    maverig.data.settings

.. contents::
    :local:

``maverig.data.config``
-----------------------

.. automodule:: maverig.data.config
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.data.dataHandler``
----------------------------

.. automodule:: maverig.data.dataHandler
    :members:
    :undoc-members:
    :show-inheritance:
