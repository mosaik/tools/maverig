=====================
maverig.views
=====================

.. toctree::
    maverig.views.groups
    maverig.views.items
    maverig.views.positioning

.. contents::
    :local:

``maverig.views.abstractView``
------------------------------

.. automodule:: maverig.views.abstractView
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.views.attributePanelView``
------------------------------------

.. automodule:: maverig.views.attributePanelView
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.views.modePanelView``
-------------------------------

.. automodule:: maverig.views.modePanelView
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.views.componentWizardView``
-------------------------------------

.. automodule:: maverig.views.componentWizardView
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.views.consolePanelView``
----------------------------------

.. automodule:: maverig.views.consolePanelView
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.views.dialogs``
-------------------------

.. automodule:: maverig.views.dialogs
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.views.mainWindow``
----------------------------

.. automodule:: maverig.views.mainWindow
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.views.menuBarView``
-----------------------------

.. automodule:: maverig.views.menuBarView
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.views.progressView``
------------------------------

.. automodule:: maverig.views.progressView
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.views.propertyPanelView``
-----------------------------------

.. automodule:: maverig.views.propertyPanelView
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.views.scenarioPanelView``
-----------------------------------

.. automodule:: maverig.views.scenarioPanelView
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.views.settingsView``
------------------------------

.. automodule:: maverig.views.settingsView
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.views.statusBarView``
-------------------------------

.. automodule:: maverig.views.statusBarView
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.views.toolbarView``
-----------------------------

.. automodule:: maverig.views.toolbarView
    :members:
    :undoc-members:
    :show-inheritance:
