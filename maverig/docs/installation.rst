============
Installation
============

This guide contains detailed installation instructions for :ref:`linux_installation` and  
:ref:`windows_installation`. A detailed guide for OSX will be added soon. If you're not proficient with
installation procedures in Python, we suggest that you stick close to the installation guide.


.. _linux_installation:

Linux
=====

This guide is based on *(K)ubuntu 14.04 Trusty Tahr, 64bit*. It works also on 
*(K)ubuntu 16.04 Xenial Xerus, 64bit*, the only difference is the installation of PySide which 
is a little bit tricky in both cases.

Maverig comes with a demo scenario which can be used to check if the installation is working
correctly. The maverig demo requires some simulators included in `mosaik-demo`__. For that
we install mosaik-demo first.

As common in Python we use `virtualenv`__ to create virtual Python environments to keep the 
dependencies required by different projects in separate places. You can find more information
in this `guide to virtualenv`__. We will install maverig and
mosaik-demo into a virtualenv called :samp:`maverig`. We also need a folder where we put maverig and mosaik-demo.
Put these folders were you usually keep your source code. In the following we assume that this is a folder called
:file:`~/Code/`.

__ http://mosaik.readthedocs.org/en/latest/installation.html
__ https://virtualenv.pypa.io/en/stable/
__ http://python-guide-pt-br.readthedocs.io/en/latest/dev/virtualenvs/

Installing mosaik-demo
----------------------

1. Maverig and the mosaik-demo require `Python`__ >= 3.3. Ubuntu ships with Python 3.4, so this 
   is okay. We also need `pip`__, a package manager for Python packages and *virtualenv* as 
   mentioned above:

   .. code-block:: bash

      $ wget https://bootstrap.pypa.io/get-pip.py
      $ sudo python get-pip.py
      $ sudo pip install -U virtualenv

__ https://www.python.org/
__ https://pip.readthedocs.org/

2. Mosaik-demo requires the libraries *NumPy*, *SciPy* and *h5py*. We also need to install the 
   version control tool *Mercurial*. You can use the packages shipped with Ubuntu. We use 
   :program:`apt-get` to install NumPy, SciPy, and h5py as well as Mercurial.

   .. code-block:: bash

      $ sudo apt-get install mercurial python3-numpy python3-scipy python3-h5py

3. Now we need to create and activate a virtual environment for maverig and its dependencies.
   The common location for virtualenvs is :file:`~/.virtualenvs/`:

   .. code-block:: bash

      $ virtualenv -p /usr/bin/python3 --system-site-packages ~/.virtualenvs/maverig
      $ source ~/.virtualenvs/maverig/bin/activate

   Your command line prompt should now start with :samp:`(maverig)` and roughly look
   like this: ``(maverig)user@kubuntu:~$``. The flag *--system-site-packages* makes sure that
   the packages installed with :program:`apt-get` can be used inside the virtualenv. 
   The *-p* option makes sure that a Python3.4 interpreter is used inside the virtualenv. 
   Without it the standard interpreter, which is Python2.7 in Ubuntu 14.04, would be used.

4. You can now get the `mosaik-demo repository`__ by cloning it into a folder where you
   store all your code and repositories. As mentioned above we'll use :file:`~/Code/`:

   .. code-block:: bash

      (mosaik)$ mkdir ~/Code
      (mosaik)$ hg clone https://bitbucket.org/mosaik/mosaik-demo ~/Code/mosaik-demo

__ https://bitbucket.org/mosaik/mosaik-demo 


5. Now we need to install all requirements (mosaik and the simulators) for mosaik-demo and
   can finally run the demo:

   .. code-block:: bash

      (mosaik)$ cd ~/Code/mosaik-demo/
      (mosaik)$ pip install -r requirements.txt
      (mosaik)$ python demo.py

   If no errors occur, the last command will start the demo. The web visualisation
   shows the demo in your browser at http://localhost:8000. You can click the nodes of the 
   topology graph to show a time series of their values. You can also drag them 
   around to rearrange them.
   
   You can cancel the simulation by pressing :kbd:`Ctrl-C`.

Installing maverig
------------------

1a. **This point is only for (K)Ubuntu 14.04. For (K)Ubuntu 16.04 refer to 1b.** 
    Maverig requires `PySide`__, a Python binding for `Qt`__. Unfortunately the required 
    version of PySide (1.2.2) is not available for Ubunutu 14.04 via :program:`apt-get` so we 
    have to build it ``manually´´. The build requires a number of prerequisits that we can 
    install using :program:`apt-get`:

    .. code-block:: bash

       (maverig)$ sudo apt-get install python3-dev build-essential cmake libqt4-dev libphonon-dev python2.7-dev libxml2-dev libxslt1-dev qtmobility-dev

__ https://wiki.qt.io/PySide
__ https://en.wikipedia.org/wiki/Qt_%28software%29

    Now we can build PySide in our virtualenv.

    .. code-block:: bash

       (maverig)$ pip install PySide==1.2.2

    The build requires some time, don't get impatient.

    For some reason PySide's post install script is not executed properly, so we have to do it
    manually. If you installed everything on its default locations, as described above, you execute
    the script as shown below, else you have to adapt your path.

    .. code-block:: bash

       (maverig)$ python ~/.virtualenvs/maverig/bin/pyside_postinstall.py -install

1b. **This point is only for (K)Ubuntu 16.04. For (K)Ubuntu 14.04 refer to 1a.**

    Maverig requires `PySide`__, a Python binding for `Qt`__. The required 
    version of PySide (1.2.2) is available for Ubunutu 14.04 via :program:`apt-get`:

    .. code-block:: bash

       $ sudo apt-get install python3-pyside

    For some reason :file:`egg-info` isn't installed properly so that pip cannot find the installed
    PySide version. We have to install the :file:`egg-info` manually. We find it in the
    PySide distribution.

    .. code-block:: bash

       $ cd ~/Code
       $ wget https://pypi.python.org/packages/source/P/PySide/PySide-1.2.2.tar.gz

    Extract the source distribution

    .. code-block:: bash

       $ tar -xvzf PySide-1.2.2.tar.gz

    Now we have to copy the egg-info into the site-distribtution folder. If you followed the
    standard rules and everything is installed in default locations you can use the following 
    command, otherwise you have to adapt your path.

    .. code-block:: bash

       $ sudo cp -r ~/Code/PySide-1.2.2/pyside_package/PySide.egg-info /usr/lib/python3/dist-packages/

__ https://wiki.qt.io/PySide
__ https://en.wikipedia.org/wiki/Qt_%28software%29


2. From this step on the guide is for both Ubuntu versions, 14.04 and 16.04. Next step is to 
   get the maverig source code and to install the remaining requirements.

   .. code-block:: bash

      (maverig)$ hg clone https://bitbucket.org/mosaik/maverig ~/Code/maverig
      (maverig)$ cd ~/Code/maverig
      (maverig)$ pip install -r requirements.txt

3. After all these preparations we are finally able to install maverig itself.

   .. code-block:: bash

      (maverig)$ pip install maverig

4. If no error occured we can now start maverig.

   .. code-block:: bash

      (maverig)$ maverig

   You'll find a demo scenario in maverig (file|open, go to folder scenarios, select demo.mvrg). If you open it
   and start the simulation (simulation|run or F5) you see the animation. You can click on
   any element to see further information about it on the left side. For the use of 
   maverig consult the :doc:`manual <user_manual>`.


.. _windows_installation:

Windows
=======

This guide is based on *Windows 7 Professional 64 bit*.

Maverig comes with a demo scenario which can be used to check if the installation is working
correctly. The maverig demo requires some simulators included in `mosaik-demo`__. For this,
we install mosaik-demo first.

As common in Python we use `virtualenv`__ to create virtual Python environments to keep the 
dependencies required by different projects in separate places. You can find more information
in this `guide to virtualenv`__. We will install maverig and
mosaik-demo into a virtualenv called :samp:`maverig`. We also need a folder where we put maverig and mosaik-demo.
Put these folders where you usually keep your source code. In the following we assume that this is a folder called
:file:`Code` that is located in your user directory.

__ http://mosaik.readthedocs.org/en/latest/installation.html
__ https://virtualenv.pypa.io/en/stable/
__ http://python-guide-pt-br.readthedocs.io/en/latest/dev/virtualenvs/

Installing mosaik demo
----------------------

1. Mosaik and the demo scenario require `Python`__ 3.4. You can find the 
   Windows installer `here`__. Select the correct Python 3.4 version for your operating system (32 or 64 Bit).

   1. When the download finished, double-click the installer.

   2. Select *Install for all users* and click *Next >*.

   3. The default installation path is okay. Make sure that the installation path does not
      contain any blanks. Click *Next >*.

   4. In the *Customize Python* page, click on the *Python* node and select
      *Entire feature will be installed on local hard drive*. Make sure that
      *Add python.exe to Path* is enabled. Click *Next >*.

   5. When Windows asks you to allow the installation, allow the installation.
      Wait. Click *Finish*.

   This also installs the Python package manager `pip`__. 


__ https://www.python.org
__ https://www.python.org/downloads/windows/
__ https://pip.readthedocs.org/

2. Download and install `Mercurial`__. We recommend to use `TortoiseHg`__. Download the
   correct version for your operation system and follow the installation assistant.


__ https://www.mercurial-scm.org/
__ https://tortoisehg.bitbucket.io/

3. The version of pip that comes with Python 3.4 is quite old, so we have to update it.
   Open a terminal window: Press the :kbd:`Windows` key (or click on the start menu) and enter ``cmd``.
   Your terminal prompt should look like ``C:\Users\yourname>``.

   Upgrade pip by:

   .. code-block:: bat
      
      C:\Users\yourname>python -m pip install --upgrade pip


   Make sure that Python and Mercurial is installed correctly by checking its versions.

   .. code-block:: bat

      C:\Users\yourname> python --version
      C:\Users\yourname> pip --version
      C:\Users\yourname> hg --version

   Depending on the versions you installed your console window should show something like this:

   .. code-block:: bat

      C:\Users\yourname>python --version
      Python 3.4.4

      C:\Users\yourname>pip --version
      pip 9.0.1 from c:\python34\lib\site-packages (python 3.4)

      C:\Users\fschloegl>hg --version
      Mercurial Distributed SCM (version 4.2.1)
      (see https://mercurial-scm.org for more information)

      Copyright (C) 2005-2017 Matt Mackall and others
      This is free software; see the source for copying conditions. There is NO
      warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

   If you get error messages the program(s) is(are) not installed correctly. In 
   most cases the environment variable `PATH`__ is not set correctly.


__ https://en.wikipedia.org/wiki/PATH_%28variable%29

4. As mentioned above we need to create a virtualenv called :file:`maverig`.
   The common location for virtualenvs is under :file:`Envs/` in your users
   directory:

   .. code-block:: bat

      C:\Users\yourname> pip install -U virtualenv
      C:\Users\yourname> virtualenv -p c:\python34\python.exe Envs\maverig
      C:\Users\yourname> Envs\maverig\Scripts\activate.bat

   You can drop the command line argument :program:`-p` if you have only one installed version
   of Python. If you have several versions of Python the path after :program:`-p` has to 
   point at the respective Python executable.

   Your command line prompt should now start with "(maverig)" and roughly look
   like this: ``(maverig) C:\Users\yourname>``.

   .. note::

      If your Windows account type is *Standard User*, you need to open the
      terminal with administarator privileges (right-click the Terminal icon,
      then *open as Administrator*). Make then sure that you are in your user
      directory:

      .. code-block:: bat

         C:\Windows\system32> cd \Users\yourname
         C:\Users\yourname>

5. Mosaik uses `PYPOWER`__ as grid simulator which requires *NumPy* and *SciPy*.
   Further we need *h5py* for the database adapter of the demo.
   
   Christoph Gohlke `provides`__ installers for them (`NumPy`__,
   `SciPy`__, `h5py`__). Select the appropriate files for your Python
   installation (32bit or 64bit, Python version), e.g. for Windows 64bit and Python 3.4:
   *numpy-1.11.3+mkl-cp34-cp34m-win_amd64.whl*,
   *scipy-0.19.1-cp34-cp34m-win_amd64.whl*, 
   *h5py-2.7.0-cp34-cp34m-win_amd64.whl*.

   Use a numpy-version <= 1.11! Numpy 1.12 or 1.13 don't work.

   .. note::

      Run ``python -c "import sys; print(sys.version)"`` from the command prompt
      in order to get the system architecture and Python version.

      If you have a 64bit Windows, but installed a 32bit Python, also use
      the 32bit versions of NumPy etc.

   Download them into your downloads folder and install them via the following
   commands:

   .. code-block:: bat

      (maverig) C:\Users\yourname> pip install Downloads\numpy-1.11.3+mkl-cp34-cp34m-win_amd64.whl
      (maverig) C:\Users\yourname> pip install Downloads\scipy-0.19.1-cp34-cp34m-win_amd64.whl
      (maverig) C:\Users\yourname> pip install Downloads\h5py-2.7.0-cp34-cp34m-win_amd64.whl

__ https://github.com/rwl/PYPOWER
__ http://www.lfd.uci.edu/~gohlke/pythonlibs/
__ http://www.lfd.uci.edu/~gohlke/pythonlibs/#numpy
__ http://www.lfd.uci.edu/~gohlke/pythonlibs/#scipy
__ http://www.lfd.uci.edu/~gohlke/pythonlibs/#h5py

6. Create a folder for your code (if none exists yet) and clone the mosaik-demo repository into it:

   .. code-block:: bat

      (maverig)C:\Users\yourname> mkdir Code
      (maverig)C:\Users\yourname> cd Code
      (maverig)C:\Users\yourname\Code> hg clone https://bitbucket.org/mosaik/mosaik-demo

7. Now we only need to install all requirements (mosaik and the simulators) and
   can finally run the demo:

   .. code-block:: bat

      (maverig)C:\Users\yourname\Code> cd mosaik-demo
      (maverig)C:\Users\yourname\Code\mosaik-demo> pip install -r requirements.txt
      (maverig)C:\Users\yourname\Code\mosaik-demo> python demo.py

   If no errors occur, the last command will start the demo. The web visualisation
   shows the demo in your browser at http://localhost:8000. You can click the nodes of the 
   topology graph to show a time series of their values. You can also drag them 
   around to rearrange them.
   
   You can cancel the simulation by pressing :kbd:`Ctrl-C`.


Installing maverig
------------------

1. Clone the maverig repository into your :file:`Code` folder:

   .. code-block:: bat

      (maverig)C:\Users\yourname\Code\mosaik-demo> cd C:\Users\yourname\Code
      (maverig)C:\Users\yourname\Code> hg clone https://bitbucket.org/mosaik/maverig

2. Install all requirements of maverig:

   .. code-block:: bat

      (maverig)C:\Users\yourname\Code> cd maverig
      (maverig)C:\Users\yourname\Code\maverig> pip install -r requirements.txt

3. After all these preparations we are finally able to install maverig itself.

   .. code-block:: bat

      (maverig)C:\Users\yourname\Code\maverig> pip install maverig

4. If no error occured we can now start maverig.

   .. code-block:: bat

      (maverig)$ C:\Users\yourname\Code\maverig> cd maverig
      (maverig)$ C:\Users\yourname\Code\maverig> python EntryPoint.py

   You'll find a demo scenario in maverig (file|open, go to folder scenarios, select demo.mvrg). If you open it
   and start the simulation (simulation|run or F5) you see the animation. You can click on
   any element to see further information about it on the left side. For the use of 
   maverig consult the :doc:`manual <user_manual>`.

