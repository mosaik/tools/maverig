==========
Quickstart
==========

Maverig runs on the operation systems Linux, OSX and Windows. It requires `Python 3.4`__ or 
higher and the package manager `pip`__.

__ https://www.python.org/downloads/release/python-343/
__ https://pypi.python.org/pypi/pip

Maverig can simply be installed with the command:

.. code-block:: bash

    $ pip install maverig

However, you need to install some requirements beforehand:

.. include:: ../../requirements.txt
    :literal:

The installation of *mosaik*, *mosaik-api* and *mosaik_pypower* is described in detail in the 
`mosaik installation guide`__. There is a detailed :ref:`installation guide for Linux <linux_installation>` (Ubuntu) and some 
hints for :ref:`installation under Windows <windows_installation>`. A detailed guide for Windows and OSX will be added soon.

__ http://mosaik.readthedocs.org/en/latest/installation.html
