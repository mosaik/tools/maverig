=============================
maverig.data.settings
=============================

.. contents::
    :local:

``maverig.data.settings.abstractSettings``
-----------------------------------------

.. automodule:: maverig.data.settings.abstractSettings
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.data.settings.defaultSettings``
-----------------------------------------

.. automodule:: maverig.data.settings.defaultSettings
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.data.settings.heatValueEffect``
-----------------------------------------

.. automodule:: maverig.data.settings.heatValueEffect
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.data.settings.settings``
----------------------------------

.. automodule:: maverig.data.settings.settings
    :members:
    :undoc-members:
    :show-inheritance:
