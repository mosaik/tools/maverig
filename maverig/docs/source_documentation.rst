====================
Source Documentation
====================

The Source Documentation describes classes and methods
in Maverig source code at https://bitbucket.org/Sash221/maverig/src.

This may help you to understand the software in detail
or if you plan further development of additional features or component descriptions.

.. toctree::

    maverig.data
    maverig.models
    maverig.presenter
    maverig.views
    maverig.utils
    maverig.tests


``maverig.EntryPoint``
----------------------

.. automodule:: maverig.EntryPoint
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.demo``
----------------

.. automodule:: maverig.demo
    :members:
    :undoc-members:
    :show-inheritance:
