=========================================
maverig.presenter.group_presenter
=========================================

.. contents::
    :local:

``maverig.presenter.group_presenter.abstractGroupPresenter``
------------------------------------------------------------

.. automodule:: maverig.presenter.group_presenter.abstractGroupPresenter
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.presenter.group_presenter.iconGroupPresenter``
--------------------------------------------------------

.. automodule:: maverig.presenter.group_presenter.iconGroupPresenter
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.presenter.group_presenter.lineGroupPresenter``
--------------------------------------------------------

.. automodule:: maverig.presenter.group_presenter.lineGroupPresenter
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.presenter.group_presenter.lineIconGroupPresenter``
------------------------------------------------------------

.. automodule:: maverig.presenter.group_presenter.lineIconGroupPresenter
    :members:
    :undoc-members:
    :show-inheritance:

``maverig.presenter.group_presenter.nodeGroupPresenter``
--------------------------------------------------------

.. automodule:: maverig.presenter.group_presenter.nodeGroupPresenter
    :members:
    :undoc-members:
    :show-inheritance:
