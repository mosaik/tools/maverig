default_settings = {
    "ui_state": {
        "is_console_panel_visible": True,
        "is_status_bar_visible": True,
        "is_attribute_panel_visible": True,
        "is_property_panel_visible": True,
        "main_window_geometry": "AdnQywABAAD////4////+AAAB4cAAAQXAAAB6AAAAWIAAAKvAAAC5wAAAAACAA==",
        "splitter_right_state": "AAAA/wAAAAAAAAAEAAAAVwAAAtgAAAAAAAAAtgEAAAAFAQAAAAI=",
        "splitter_right_geometry": "AdnQywABAAAAAAGDAAAAAAAAB20AAAOwAAAAAAAAAAD//////////wAAAAAAAA==",
        "splitter_left_state": "AAAA/wAAAAAAAAADAAAC9gAAALcAAAAAAQAAAAQBAAAAAg==",
        "splitter_main_state": "AAAA/wAAAAAAAAACAAABfgAABesBAAAABQEAAAAB",
        "is_progress_bar_visible": True,
        "splitter_left_geometry": "AdnQywABAAAAAAAAAAAAAAAAAX0AAAOwAAAAAAAAAAD//////////wAAAAAAAA==",
        "is_component_panel_visible": True,
        "splitter_main_geometry": "AdnQywABAAAAAAAJAAAACQAAB3YAAAO5AAAAAAAAAAD//////////wAAAAAAAA==",
        "main_window_state": "AAAA/wAAAAD9AAAAAAAAB4AAAAPDAAAABAAAAAQAAAAIAAAACPwAAAALAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAIAAAAAAAAAAgAAAAAAAAACAAAAAAAAAAIAAAAAAAAAAgAAAAAAAAACAAAAAQAAAA4AbQBhAHYAZQByAGkAZwEAAAAA/////wAAAAAAAAAA",
        "attribute_graphs_visible": ["P"]
    },
    "simulation_settings": {
        "heat_value_effect_grids": "Shadow",
        "heat_value_effect_cpp": "Bar",
        "is_heat_value_effect_for_grids_enabled": True,
        "is_heat_value_effect_for_cpp_enabled": True,
        "is_day_night_vis_enabled": True
    },
    "general_settings": {
        "language": "en_EN"
    },
    "mode_panel_settings": {
        "invisible_components": [],
        "show_invisible_components": False
    }
}