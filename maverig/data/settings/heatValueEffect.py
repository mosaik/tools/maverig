
class HeatValueEffectKeys():
    # heat_value_effect settings
    EFFECT_BAR = "Bar"
    EFFECT_BLUR = "Blur"
    EFFECT_COLOR = "Color"
    EFFECT_SHADOW = "Shadow"
    EFFECT_TRANSPARENCY = "Transparency"